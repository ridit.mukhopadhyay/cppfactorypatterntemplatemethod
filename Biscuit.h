/*
 * Biscuit.h
 *
 *  Created on: 16-Mar-2021
 *      Author: ridit
 */

#ifndef BISCUIT_H_
#define BISCUIT_H_

#include "Food.h"

namespace somitsolutions {

class Biscuit: public Food {
public:
	Biscuit();
	virtual ~Biscuit();
	string getName();
};

} /* namespace somitsolutions */

#endif /* BISCUIT_H_ */
