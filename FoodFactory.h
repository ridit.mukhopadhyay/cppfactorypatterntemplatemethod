/*
 * FoodFactory.h
 *
 *  Created on: 16-Mar-2021
 *      Author: ridit
 */

#ifndef FOODFACTORY_H_
#define FOODFACTORY_H_

#include <iostream>


using namespace std;
namespace somitsolutions {

class FoodFactory {

public:

	template<typename T, typename... Params>
	static unique_ptr<T> create (Params... params){
		return std :: make_unique<T>(params...);
	}
};

} /* namespace somitsolutions */

#endif /* FOODFACTORY_H_ */
