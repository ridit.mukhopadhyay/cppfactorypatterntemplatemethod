/*
 * Chocolate.h
 *
 *  Created on: 16-Mar-2021
 *      Author: ridit
 */

#ifndef CHOCOLATE_H_
#define CHOCOLATE_H_

#include "Food.h"

namespace somitsolutions {

class Chocolate: public Food {
public:
	Chocolate();
	virtual ~Chocolate();
	string getName();

};

} /* namespace somitsolutions */

#endif /* CHOCOLATE_H_ */
