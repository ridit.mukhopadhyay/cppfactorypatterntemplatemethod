/*
 * Food.h
 *
 *  Created on: 16-Mar-2021
 *      Author: ridit
 */

#ifndef FOOD_H_
#define FOOD_H_

#include <iostream>
#include <string>

using namespace std;
namespace somitsolutions {

class Food {
public:
	Food();
	virtual ~Food();
	virtual string getName();
};

} /* namespace somitsolutions */

#endif /* FOOD_H_ */
