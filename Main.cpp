//============================================================================
// Name        : FactoryPattern.cpp
// Author      : Ridit
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <memory>
#include "Chocolate.h"
#include "Biscuit.h"
#include "FoodFactory.h"
using namespace std;

int main() {

	using namespace somitsolutions;

	shared_ptr<Biscuit> bi = FoodFactory::create<Biscuit>();
	shared_ptr<Chocolate> ch = FoodFactory::create<Chocolate>();

	cout<<ch->getName()<<endl;

	cout<<bi->getName() <<endl;


	return 0;
}
